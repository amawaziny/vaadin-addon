/*
 *  Copyright 2016 QFast Ahmed El-mawaziny.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.qfast.vaadin.addon;

import com.vaadin.data.Property;
import com.vaadin.ui.TextField;
import org.qfast.vaadin.addon.client.qtextfield.QTextFieldState;

/**
 * @author Ahmed El-mawaziny
 */
public class QTextField extends TextField {
    private static final long serialVersionUID = -4040366491760139848L;

    public QTextField() {
        super();
        setup();
    }

    public QTextField(Property<?> dataSource) {
        super(dataSource);
        setup();
    }

    public QTextField(Property<?> dataSource, boolean flatting) {
        super(dataSource);
        setFlatting(flatting);
        setup();
    }

    public QTextField(String caption, Property<?> dataSource) {
        super(caption, dataSource);
        setup();
    }

    public QTextField(String caption, Property<?> dataSource, boolean flatting) {
        super(caption, dataSource);
        setFlatting(flatting);
        setup();
    }

    public QTextField(String caption, String value) {
        super(caption, value);
        setup();
    }

    public QTextField(String caption, String value, boolean flatting) {
        super(caption, value);
        setFlatting(flatting);
        setup();
    }

    public QTextField(String caption) {
        super(caption);
        setup();
    }

    public QTextField(String caption, boolean flatting) {
        super(caption);
        setFlatting(flatting);
        setup();
    }

    private void setup() {
        setNullRepresentation("");
        setNullSettingAllowed(false);
        setValidationVisible(false);
    }

    @Override
    protected QTextFieldState getState() {
        return (QTextFieldState) super.getState();
    }

    /**
     * @return placeholder
     */
    public String getPlaceholder() {
        return getState().placeholder;
    }

    /**
     * @param placeholder placeholder to be entered
     */
    public void setPlaceholder(String placeholder) {
        getState().placeholder = placeholder;
    }

    /**
     * @return flatting
     */
    public boolean isFlatting() {
        return getState().flatting;
    }

    /**
     * @param flatting
     */
    public void setFlatting(boolean flatting) {
        getState().flatting = flatting;
        String placeholder = getPlaceholder();
        if (flatting && (placeholder == null || placeholder.isEmpty())) {
            setPlaceholder(getCaption());
        }
    }
}
