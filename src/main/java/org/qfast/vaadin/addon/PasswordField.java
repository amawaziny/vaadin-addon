/*
 *  Copyright 2016 QFast Ahmed El-mawaziny.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.qfast.vaadin.addon;

import com.vaadin.data.Property;
import org.qfast.vaadin.addon.client.passwordfield.PasswordFieldState;

/**
 * @author Ahmed El-mawaziny
 */
public class PasswordField extends com.vaadin.ui.PasswordField {

    private static final long serialVersionUID = -5746942315022656039L;

    public PasswordField() {
        super();
        setup();
    }

    public PasswordField(Property<?> dataSource) {
        super(dataSource);
        setup();
    }

    public PasswordField(String caption, Property<?> dataSource) {
        super(caption, dataSource);
        setup();
    }

    public PasswordField(String caption, String value) {
        super(caption, value);
        setup();
    }

    public PasswordField(String caption) {
        super(caption);
        setup();
    }

    private void setup() {
        setNullRepresentation("");
        setNullSettingAllowed(false);
        setValidationVisible(false);
    }

    @Override
    protected PasswordFieldState getState() {
        return (PasswordFieldState) super.getState();
    }

    /**
     * @return placeholder
     */
    public String getPlaceholder() {
        return getState().placeholder;
    }

    /**
     * @param placeholder placeholder to be entered
     */
    public void setPlaceholder(String placeholder) {
        getState().placeholder = placeholder;
    }
}
