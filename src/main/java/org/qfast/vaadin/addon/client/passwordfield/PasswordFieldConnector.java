/*
 *  Copyright 2016 QFast Ahmed El-mawaziny.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.qfast.vaadin.addon.client.passwordfield;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.shared.ui.Connect;
import org.qfast.vaadin.addon.PasswordField;

/**
 * @author Ahmed El-mawaziny
 */
@Connect(PasswordField.class)
public class PasswordFieldConnector extends com.vaadin.client.ui.passwordfield.PasswordFieldConnector {

    private static final long serialVersionUID = 3826845215110497889L;

    @Override
    protected Widget createWidget() {
        return GWT.create(PasswordFieldWidget.class);
    }

    @Override
    public PasswordFieldWidget getWidget() {
        return (PasswordFieldWidget) super.getWidget();
    }

    @Override
    public PasswordFieldState getState() {
        return (PasswordFieldState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        getWidget().setPlaceholder(getState().placeholder);
        getWidget().setValue(getState().text);
    }
}
