/*
 *  Copyright 2015 QFast Ahmed El-mawaziny.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.qfast.vaadin.addon.client.qtextfield;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.textfield.TextFieldConnector;
import com.vaadin.shared.ui.Connect;
import org.qfast.vaadin.addon.QTextField;

/**
 * @author Ahmed El-mawaziny
 */
@Connect(QTextField.class)
public class QTextFieldConnector extends TextFieldConnector {
    private static final long serialVersionUID = 1257896132356472343L;

    @Override
    protected Widget createWidget() {
        return GWT.create(QTextFieldWidget.class);
    }

    @Override
    public QTextFieldWidget getWidget() {
        return (QTextFieldWidget) super.getWidget();
    }

    @Override
    public QTextFieldState getState() {
        return (QTextFieldState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        final QTextFieldWidget widget = getWidget();
        QTextFieldState state = getState();

        widget.setPlaceholder(state.placeholder);
        widget.setFlatting(state.flatting);
        widget.setValue(state.text);

        if (state.flatting) {
            final Widget parent = widget.getParent();
            widget.addKeyUpHandler(new KeyUpHandler() {
                @Override
                public void onKeyUp(KeyUpEvent keyUpEvent) {
                    String value = widget.getValue();
                    if (value == null || value.isEmpty()) {
                        parent.addStyleName(QTextFieldWidget.CLASSNAME);
                        parent.removeStyleName(QTextFieldWidget.CLASSNAME_FILLED);
                        parent.removeStyleName(QTextFieldWidget.CLASSNAME_FILLED_NO_ANIMATION);
                    } else {
                        switch (keyUpEvent.getNativeKeyCode()) {
                            case KeyCodes.KEY_LEFT:
                            case KeyCodes.KEY_RIGHT:
                            case KeyCodes.KEY_BACKSPACE:
                            case KeyCodes.KEY_DELETE:
                            case KeyCodes.KEY_TAB:
                            case KeyCodes.KEY_UP:
                            case KeyCodes.KEY_DOWN:
                            case KeyCodes.KEY_SHIFT:
                                return;
                        }
                        parent.removeStyleName(QTextFieldWidget.CLASSNAME);
                        parent.addStyleName(QTextFieldWidget.CLASSNAME_FILLED);
                    }
                }
            });
            widget.addBlurHandler(new BlurHandler() {
                @Override
                public void onBlur(BlurEvent blurEvent) {
                    String value = widget.getValue();
                    parent.removeStyleName(QTextFieldWidget.CLASSNAME_FOUCS);
                    parent.removeStyleName(QTextFieldWidget.CLASSNAME_FILLED);
                    if (value == null || value.isEmpty()) {
                        parent.addStyleName(QTextFieldWidget.CLASSNAME);
                    } else {
                        parent.addStyleName(QTextFieldWidget.CLASSNAME_FILLED_NO_ANIMATION);
                    }
                }
            });
            widget.addFocusHandler(new FocusHandler() {
                @Override
                public void onFocus(FocusEvent focusEvent) {
                    parent.addStyleName(QTextFieldWidget.CLASSNAME_FOUCS);
                }
            });
        }
    }
}
