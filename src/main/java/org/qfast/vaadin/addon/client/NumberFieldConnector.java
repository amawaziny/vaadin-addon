/*
 * Copyright 2014 QFast Ahmed El-mawaziny.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.qfast.vaadin.addon.client;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ServerConnector;
import com.vaadin.client.extensions.AbstractExtensionConnector;
import com.vaadin.client.ui.VTextField;
import com.vaadin.shared.ui.Connect;
import org.qfast.vaadin.addon.NumberField;

import java.math.BigDecimal;

/**
 * @author Ahmed El-mawaziny
 */
@Connect(NumberField.class)
public class NumberFieldConnector extends AbstractExtensionConnector {

    private static final long serialVersionUID = 4801082235612276424L;
    private final ContextMenuHandler contextMenuHandler = new ContextMenuHandler() {

        @Override
        public void onContextMenu(ContextMenuEvent event) {
            event.preventDefault();
            event.stopPropagation();
        }
    };
    private VTextField textField;
    private final KeyPressHandler keyPressHandler = new KeyPressHandler() {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (textField.isReadOnly() || !textField.isEnabled()) {
                return;
            }
            int keyCode = event.getNativeEvent().getKeyCode();
            switch (keyCode) {
                case KeyCodes.KEY_LEFT:
                case KeyCodes.KEY_RIGHT:
                case KeyCodes.KEY_BACKSPACE:
                case KeyCodes.KEY_DELETE:
                case KeyCodes.KEY_TAB:
                case KeyCodes.KEY_UP:
                case KeyCodes.KEY_DOWN:
                case KeyCodes.KEY_SHIFT:
                    return;
            }
            if (!isValueValid(event)) {
                textField.cancelKey();
            }
        }
    };
    private final BlurHandler blurHandler = new BlurHandler() {

        @Override
        public void onBlur(BlurEvent event) {
            String oldValue = textField.getValue();
            textField.setValue(oldValue.replaceAll("[^0-9.]", ""));
            if(!isValueValid(textField.getValue())){
                textField.setValue("");
            }
        }
    };

    @Override
    protected void extend(ServerConnector target) {
        textField = (VTextField) ((ComponentConnector) target).getWidget();
        textField.addKeyPressHandler(keyPressHandler);
        textField.addDomHandler(contextMenuHandler, ContextMenuEvent.getType());
        textField.addBlurHandler(blurHandler);
    }

    private boolean isValueValid(KeyPressEvent event) {
        String newText = getValueAfterKeyPress(event.getCharCode());
        return isValueValid(newText);
    }

    private boolean isValueValid(String s) {
        try {
            parseValue(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected BigDecimal parseValue(String value) {
        NumberFormat nf = NumberFormat.getDecimalFormat();
        return BigDecimal.valueOf(nf.parse(value));
    }

    private String getValueAfterKeyPress(char charCode) {
        int index = textField.getCursorPos();
        String previousText = textField.getText();
        StringBuilder buffer = new StringBuilder();
        buffer.append(previousText.substring(0, index));
        buffer.append(charCode);
        if (textField.getSelectionLength() > 0) {
            buffer.append(previousText.substring(index + textField.getSelectionLength(),
                    previousText.length()));
        } else {
            buffer.append(previousText.substring(index, previousText.length()));
        }
        return buffer.toString();
    }
}
