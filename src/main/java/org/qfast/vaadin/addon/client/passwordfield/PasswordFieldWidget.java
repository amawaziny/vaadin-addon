/*
 *  Copyright 2016 QFast Ahmed El-mawaziny.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.qfast.vaadin.addon.client.passwordfield;

import com.vaadin.client.ui.VPasswordField;

/**
 * @author Ahmed El-mawaziny
 */
public class PasswordFieldWidget extends VPasswordField {

    public PasswordFieldWidget() {
    }

    public void setPlaceholder(String placeholder) {
        if (placeholder == null) {
            getElement().setPropertyString("placeholder", "");
        } else {
            getElement().setPropertyString("placeholder", placeholder);
        }
    }
}
